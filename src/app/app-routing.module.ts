import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {
  CourseCardComponent,
  CourseEditCreateComponent,
  CourseFormComponent,
  LoginFormComponent,
  RegistrationFormComponent,
} from "./shared/components";
import {CoursesComponent} from "./features/courses/courses.component";

const routes: Routes = [
  {
    path: "login",
    component: LoginFormComponent,
  },
  {
    path: "register",
    component: RegistrationFormComponent,
  },
  {
    path: "courses/create",
    component: CourseFormComponent,
    // component: CourseEditCreateComponent,
  },
  {
    path: "courses",
    component: CoursesComponent,
  },
  /* Add your code here */
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
