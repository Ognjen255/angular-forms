import {Component, EventEmitter, Input, Output} from "@angular/core";
import {mockedAuthorsList, mockedCoursesList} from "@app/shared/mocks/mock";

@Component({
  selector: "app-courses-list",
  templateUrl: "./courses-list.component.html",
  styleUrls: ["./courses-list.component.scss"],
})
export class CoursesListComponent {
  @Input() courses = [];
  @Input() editable: boolean = false;

  coursesMock = mockedCoursesList;
  authorsMock = mockedAuthorsList;

  @Output() showCourse = new EventEmitter();
  @Output() editCourse = new EventEmitter();
  @Output() deleteCourse = new EventEmitter();

  onShowCourse(event: Event) {
    this.showCourse.emit();
  }

  onEditCourse(event: Event) {
    alert("edit course");
    this.editCourse.emit();
  }

  onDeleteCourse(event: Event) {
    alert("delete course");
    this.deleteCourse.emit();
  }
}
