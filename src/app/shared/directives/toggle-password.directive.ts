import {Directive, ElementRef} from "@angular/core";

@Directive({
  selector: "[appTogglePassword]",
  exportAs: "appTogglePassword",
})
export class TogglePasswordDirective {
  public isPasswordVisible: boolean = false;

  constructor(private inputElement: ElementRef<HTMLInputElement>) {}

  togglePasswordVisibility(): void {
    this.isPasswordVisible = !this.isPasswordVisible;
    this.inputElement.nativeElement.type = this.isPasswordVisible
      ? "text"
      : "password";
  }
}
