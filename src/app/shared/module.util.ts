import {AbstractControl, FormControl, FormGroup} from "@angular/forms";

export class UtilModule {
  static calculateDuration(minutes: number): string {
    const fullHours = Math.floor(minutes / 60);
    const rest = minutes % 60;

    return `${fullHours}:${rest} hours`;
  }

  static validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  static handleError(
    control: AbstractControl | null,
    fieldName: string
  ): string | null {
    if (!control || !control.errors) return null;

    const errorMessages = {
      required: (fieldName: string) => `${fieldName} is required.`,
      minlength: (fieldName: string, error: any) =>
        `${fieldName} must be at least ${error.requiredLength} characters long.`,
      min: (fieldName: string, error: any) =>
        `${fieldName} must be greater than ${error.min}`,
      pattern: (fieldName: string) =>
        `${fieldName} must only contain letters and numbers`,
      emailInvalid: () => `Please enter valid email.`,
    };

    const firstErrorKey = Object.keys(
      control.errors
    )[0] as keyof typeof errorMessages;
    const getErrorMessageFn = errorMessages[firstErrorKey];

    if (!getErrorMessageFn) return null;

    return getErrorMessageFn(fieldName, control.errors[firstErrorKey]);
  }
}
