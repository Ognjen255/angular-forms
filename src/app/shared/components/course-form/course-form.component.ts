import { Component } from "@angular/core";
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { UtilModule } from "@app/shared/module.util";
import { FaIconLibrary } from "@fortawesome/angular-fontawesome";
import { fas } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-course-form",
  templateUrl: "./course-form.component.html",
  styleUrls: ["./course-form.component.scss"],
})
export class CourseFormComponent {
  constructor(public fb: FormBuilder, public library: FaIconLibrary) {
    library.addIconPacks(fas);
    this.createForm();
  }
  courseForm!: FormGroup;
  // Use the names `title`, `description`, `author`, 'authors' (for authors list), `duration` for the form controls.

  createForm() {
    this.courseForm = this.fb.group({
      title: ["", [Validators.required, Validators.minLength(2)]],
      description: ["", [Validators.required, Validators.minLength(2)]],
      authors: this.fb.array([], Validators.required),
      courseAuthors: this.fb.array([]),
      author: [
        "",
        [Validators.minLength(2), Validators.pattern(/^[a-zA-Z0-9]*$/)],
      ],
      newAuthor: this.fb.group({
        author: [
          "",
          [Validators.minLength(2), Validators.pattern(/^[a-zA-Z0-9]*$/)],
        ],
      }),
      duration: ["", [Validators.required, Validators.min(0)]],
    });
  }

  onCreateCourse() {
    UtilModule.validateAllFormFields(this.courseForm);
  }

  onAddAuthor() {
    if (!this.author?.value) return;

    this.authors.push(
      this.fb.control({ name: this.author?.value, id: crypto.randomUUID() })
    );
    this.author?.setValue("");
  }

  onAddAuthorToCourse(newAuthor: any, index: number) {
    this.courseAuthors.push(this.fb.control(newAuthor?.value));
    this.authors.removeAt(index);
  }

  onRemoveAuthorFromCourse(author: any, index: number) {
    this.courseAuthors.removeAt(index);
    this.authors.push(author);
  }

  onRemoveAuthor(index: number) {
    this.authors.removeAt(index);
  }

  handleError(
    control: AbstractControl | null,
    fieldName: string
  ): string | null {
    return UtilModule.handleError(control, fieldName);
  }

  get author() {
    return this.courseForm.get("newAuthor.author");
  }

  get test() {
    return this.courseForm.get("newAuthor.test");
  }

  get authors(): FormArray {
    return this.courseForm.get("authors") as FormArray;
  }

  get courseAuthors(): FormArray {
    return this.courseForm.get("courseAuthors") as FormArray;
  }

  get title() {
    return this.courseForm.get("title");
  }

  get description() {
    return this.courseForm.get("description");
  }

  get duration() {
    return this.courseForm.get("duration");
  }
}
