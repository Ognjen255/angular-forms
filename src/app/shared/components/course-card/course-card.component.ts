import {Component, EventEmitter, Input, Output} from "@angular/core";
import {UtilModule} from "@app/shared/module.util";

@Component({
  selector: "app-course-card",
  templateUrl: "./course-card.component.html",
  styleUrls: ["./course-card.component.scss"],
})
export class CourseCardComponent {
  @Input() editable: boolean = false;
  @Output() clickOnShow = new EventEmitter();

  title: string = "Angular";
  description: string =
    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga rerum repudiandae consequatur similique repellat minima? Ipsam vero, accusantium molestias rerum ex soluta eum officia libero vel dolor aliquid quisquam exercitationem?";
  creationDate: Date = new Date("08/03/2021");
  duration: number = 45;
  authors: string[] = ["Dave Haisenberg", "Tony Ja"];

  durationInHours: string = UtilModule.calculateDuration(this.duration);

  constructor() {}

  onClickShow() {
    this.clickOnShow.emit();
    alert("Show course clicked");
  }
}
