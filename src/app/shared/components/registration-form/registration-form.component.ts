import {Component, OnInit} from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {trigger, style, animate, transition} from "@angular/animations";
import {UtilModule} from "@app/shared/module.util";

@Component({
  selector: "app-registration-form",
  templateUrl: "./registration-form.component.html",
  styleUrls: ["./registration-form.component.scss"],
  animations: [
    trigger("inOutAnimation", [
      transition(":enter", [
        style({height: 0, opacity: 0}),
        animate("1s ease-out", style({height: 300, opacity: 1})),
      ]),
      transition(":leave", [
        style({height: 300, opacity: 1}),
        animate("1s ease-in", style({height: 0, opacity: 0})),
      ]),
    ]),
  ],
})
export class RegistrationFormComponent implements OnInit {
  registrationForm!: FormGroup;
  // Use the names `name`, `email`, `password` for the form controls.

  ngOnInit(): void {
    this.registrationForm = new FormGroup({
      name: new FormControl(null, [
        Validators.required,
        Validators.minLength(6),
      ]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required),
    });
  }

  get name() {
    return this.registrationForm.get("name");
  }

  get email() {
    return this.registrationForm.get("email");
  }

  onSubmit(): void {
    if (this.registrationForm.valid) {
      alert("Register");
    } else {
      // validate all form fields
      UtilModule.validateAllFormFields(this.registrationForm);
    }
  }
}
