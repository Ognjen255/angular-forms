import {Component, EventEmitter, Input, Output} from "@angular/core";
import {FaIconLibrary} from "@fortawesome/angular-fontawesome";
import {fas} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-button",
  templateUrl: "./button.component.html",
  styleUrls: ["./button.component.scss"],
})
export class ButtonComponent {
  @Input() buttonType: string = "";
  @Input() buttonText: string = "Default text";
  @Input() iconName: any = "";
  @Input() isDisabled: boolean = false;

  @Output() btnClick = new EventEmitter();

  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas);
  }

  onClick(event: Event) {
    this.btnClick.emit();
  }

  // Use the names for the inputs `buttonText` and `iconName`.
}
