import {Component, ViewChild} from "@angular/core";
import {NgForm} from "@angular/forms";
import {trigger, style, animate, transition} from "@angular/animations";

@Component({
  selector: "app-login-form",
  templateUrl: "./login-form.component.html",
  styleUrls: ["./login-form.component.scss"],
  animations: [
    trigger("inOutAnimation", [
      transition(":enter", [
        style({opacity: 0}),
        animate("1s ease-out", style({opacity: 1})),
      ]),
      transition(":leave", [
        style({opacity: 1}),
        animate("1s ease-in", style({opacity: 0})),
      ]),
    ]),
  ],
})
export class LoginFormComponent {
  @ViewChild("loginForm") public loginForm!: NgForm;
  //Use the names `email` and `password` for form controls.

  email: string = "";
  password: string = "";

  onLogin() {
    if (this.loginForm.form.status === "INVALID") return;
    alert("Login");
  }
}
