import {Component, OnInit} from "@angular/core";
import {
  AbstractControl,
  FormArray,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {UtilModule} from "@app/shared/module.util";

@Component({
  selector: "app-course-edit-create-form",
  styleUrls: ["./course-edit-create.component.scss"],
  templateUrl: "./course-edit-create.component.html",
})
export class CourseEditCreateComponent implements OnInit {
  editCreateForm!: FormGroup;
  // authors: any = [];

  ngOnInit(): void {
    this.editCreateForm = new FormGroup({
      title: new FormControl(null, [
        Validators.required,
        Validators.minLength(2),
      ]),
      description: new FormControl(null, [
        Validators.required,
        Validators.minLength(2),
      ]),
      duration: new FormControl(null, [Validators.required, Validators.min(0)]),
      newAuthor: new FormGroup({
        authors: new FormArray([]),
        author: new FormControl(null, [
          Validators.minLength(2),
          Validators.pattern(/^[a-zA-Z0-9]*$/),
        ]),
      }),
    });
  }

  handleError(
    control: AbstractControl | null,
    fieldName: string
  ): string | null {
    return UtilModule.handleError(control, fieldName);
  }

  onAddAuthor() {
    console.log(this.editCreateForm);
    const newAuthor = new FormControl(this.author?.value);
    this.authors.push(newAuthor);
    this.author?.setValue("");
  }

  onDeleteAuthor(index: number) {
    this.authors.removeAt(index);
  }

  onSubmit() {
    console.log(this.editCreateForm);
  }

  get authors() {
    return this.editCreateForm.get("newAuthor.authors") as FormArray;
  }

  get title() {
    return this.editCreateForm.get("title");
  }

  get description() {
    return this.editCreateForm.get("description");
  }

  get duration() {
    return this.editCreateForm.get("duration");
  }

  get author() {
    return this.editCreateForm.get("newAuthor.author");
  }
}
