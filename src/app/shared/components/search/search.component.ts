import {Component, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {NgForm} from "@angular/forms";

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.scss"],
})
export class SearchComponent {
  // Use the name `placeholder` for the @Input.
  // Use the name `search` for the @Output.
  @ViewChild("loginForm") public searchForm!: NgForm;

  @Input() placeholder: string = "Search...";
  @Output() search = new EventEmitter();

  searchInput: string = "";

  onSearch(event: Event) {
    this.search.emit();
  }
}
