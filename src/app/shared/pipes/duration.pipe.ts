import {Pipe} from "@angular/core";

@Pipe({
  name: "duration",
})
export class DurationPipe {
  // Add your code here
  transform(value: any) {
    const fullHours = String(Math.floor(value / 60)).padStart(2, "0");
    const rest = String(value % 60).padStart(2, "0");

    return `${fullHours}:${rest}`;
  }
}
