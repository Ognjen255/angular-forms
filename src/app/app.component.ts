import {Component} from "@angular/core";
import {mockedAuthorsList, mockedCoursesList} from "./shared/mocks/mock";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  title = "courses-app";
  mockAuthors = mockedAuthorsList;
  mockCourses = mockedCoursesList;
}
